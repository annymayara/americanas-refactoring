# americanas s.a.

## Treinamento de Código Limpo e Refatoração

Código usado de exemplo para mostrar uma refatoração.

## Refatorações Usadas:

- Rename Variable
- Change Function Declaration
- Extract Function
- Replace Command with Function
- Decompose Conditional
- Replace Nested Conditional with Guard Clauses

Fonte: https://refactoring.com/catalog
