public class Exemplo {

    public static final double DESCONTO_5_PROCENTO = 0.95;
    public static final double DESCONTO_2_PROCENTO = 0.98;

    /**
     * Método que calcula preço total aplicando o desconto
     * @param quantidade quantidade de produtos
     * @param valorUnitario valor do produto
     * @return valor calculado
     */
    public Double getPreco(int quantidade, int valorUnitario) {
        return calculaValorTotal(quantidade, valorUnitario) * calculaFatorDesconto(quantidade, valorUnitario);
    }

    private double calculaFatorDesconto(int quantidade, int valorUnitario) {
        if (ehValorCaro(quantidade, valorUnitario))
            return DESCONTO_5_PROCENTO;
        else
           return DESCONTO_2_PROCENTO;
    }

    private int calculaValorTotal(int quantidade, int valorUnitario) {
        return quantidade * valorUnitario;
    }

    private boolean ehValorCaro(int quantidade, int valorUnitario) {
        return calculaValorTotal(quantidade, valorUnitario) > 1000;
    }
}
