import org.junit.Assert;
import org.junit.Test;

public class ExemploTest {

    Exemplo exemplo = new Exemplo();

    @Test
    public void testa_exemplo_calculado_correto_produto_caro() {
        Assert.assertEquals(Double.valueOf(2375.0), exemplo.getPreco(5, 500));
    }

    @Test
    public void testa_exemplo_calculado_produto_barato() {
        Assert.assertEquals(Double.valueOf(4.9), exemplo.getPreco(1, 5));
    }

}
